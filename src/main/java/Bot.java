import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

import java.io.File;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Bot {

    private String id;
    private File file;

    public Bot() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    String findId() {
        //System.out.println("findId = " + getId());
        String result = null;
        String h1 = null;
        try {
            Document doc = Jsoup.connect("https://www.sima-land.ru/" + getId()).get();
            Pattern p;
            Matcher m;
            JsonParser jsonParser;
            JsonArray jo;
            JsonElement criterio;
            String item = null;
            for (Element script : doc.select("script")) {
                //p = Pattern.compile(".*dataLayer = ([^]];]*)"); // "}}];
                p = Pattern.compile(".*dataLayer = \\[(.*?)\\}\\];"); // "}}];
                m = p.matcher(script.html());
                while (m.find()) {
                    //System.out.println("["+ m.group(1) + "}}]"); // value only
                    jsonParser = new JsonParser();
                    jo = (JsonArray) jsonParser.parse("["+ m.group(1) + "}]");
                    criterio = jo.get(0).getAsJsonObject().get("criteo");
                    for (JsonElement el : criterio.getAsJsonArray()) {
                        if (el.getAsJsonObject().get("event").getAsString().equals("viewItem")) {
                            item = el.getAsJsonObject().get("item").getAsString();
                        }
                    }
                }
                if (item != null) break;
            }
            String query = "https://www.sima-land.ru/api/v3/item/?price_wo_offers=1&id=" + item + "&fields=cart_item,id,is_weighted_goods,min_qty,max_qty,qty_multiplier,qty_rules,qty_rules_data,real_min_qty,price_max,linear_meters,custom_qty_rules_data,price,sid&expand=dataLayer";
            Document document = Jsoup.parse(new URL(query).openStream(), "UTF-8", "", Parser.xmlParser());
            JsonElement items = JsonParser.parseString(document.text());
            JsonObject itemObject = items.getAsJsonObject().get("items").getAsJsonArray().get(0).getAsJsonObject();
            JsonElement dataLayer = itemObject.get("dataLayer");
            JsonElement products = dataLayer.getAsJsonObject().get("ecommerce").getAsJsonObject().get("detail").getAsJsonObject().get("products");
            result = itemObject.get("id") + ";" + itemObject.get("sid") + ";" + itemObject.get("price") + ";" + itemObject.get("price_max") + ";" + itemObject.get("min_qty") + ";" + itemObject.get("max_qty") + ";" + products.getAsJsonArray().get(0).getAsJsonObject().get("name");
            //System.out.println(result);
        } catch (Exception e) {
            result = getId() + ";404 not found;" + "404 not found;" + "404 not found;" + "404 not found;" + "404 not found;" + "404 not found;";
            //System.out.println(result);
            e.printStackTrace();
        }
        System.out.println(result);
        return result;
    }
}

