import java.io.File;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String... args) {
        Bot bot = new Bot();

        Integer skip = Integer.valueOf(args[0]);
        File input = new File(args[1]);
        File result = new File(args[2]);
        try {
            List<String> lines = Files.readAllLines(input.toPath());
            //List<String> lines = new ArrayList<>();
            //lnes.add("819757");
            List<String> newLines = new ArrayList<>();
            newLines.add(new String("id;Артикул;Цена; Максимальная цена;min_qty;max_qty; Наименование".getBytes(), StandardCharsets.UTF_8));
            System.out.println("Loaded: " + (lines.size() - skip));
            System.out.println("Skip: " + skip);
            for (int i = skip; i < lines.size(); i++) {
                Thread.sleep(500);
                bot.setId(lines.get(i));
                newLines.add(bot.findId());
            }
            System.out.print("Loaded: " + lines.size());
            System.out.println(" Passed: " + newLines.size());
            if (lines.size() == newLines.size()) {
                System.out.println("Print result");
                Files.write(result.toPath(), newLines, Charset.forName("UTF-8"));
            }
            Thread.sleep(60000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
